package edu.com.asus.database_firebase;

import java.util.Collection;

/**
 * Created by ASUS on 6/30/2017.
 */

public class Article {
    private String id ;
    private String title;
    public Article(){

    }
    public Article(String id, String title) {
        this.id = id;
        this.title = title;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
