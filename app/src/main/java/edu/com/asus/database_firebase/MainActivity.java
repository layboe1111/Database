package edu.com.asus.database_firebase;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    FirebaseDatabase database ;
    DatabaseReference myRef ;
    EditText etTitile  ;
    Button btnAdd ;
    ListView lvlist ;
    Article article ;
    ArrayList<String> articlelist ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etTitile = (EditText) findViewById(R.id.etTitle);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        lvlist = (ListView) findViewById(R.id.lvList);
        btnAdd.setOnClickListener(this);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("articles");
        articlelist = new ArrayList<>();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this , android.R.layout.simple_list_item_1 ,articlelist);
        lvlist.setAdapter(adapter);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                articlelist.clear();
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    Article article = child.getValue(Article.class);

                    articlelist.add(article.getTitle());
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onClick(View view) {

        String id  = myRef.push().getKey();
        myRef.child(id).setValue(new Article(id , etTitile.getText().toString()));
    }
}
